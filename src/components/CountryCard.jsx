import { Link } from "react-router-dom";
function CountryCard(country) {
  return (
    <>
      <Link className="country" to={`/country/${country.country.cca3}`}>
        <img src={country.country.flags.png} alt={country.country.flags.alt} />
        <div className="country-details">
          <h2>{country.country.name.common}</h2>
          <h4>Population: {country.country.population}</h4>
          <h4>region: {country.country.region}</h4>
          <h4>Capital: {country.country.capital}</h4>
        </div>
      </Link>
    </>
  );
}

export default CountryCard;
