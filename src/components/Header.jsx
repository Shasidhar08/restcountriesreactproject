export default function Header() {
  return (
    <header className="Header">
      <h1>Where in the world?</h1>
      <label className="Mode">
        <input
          type="checkbox"
          name="mode"
          id="mode-toggle"
          style={{ display: "none" }}
        />
        <h3>Light Mode</h3>
      </label>
    </header>
  );
}
