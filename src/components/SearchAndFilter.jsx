import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import Languages from "../pages/Languages";

function SearchAndFilter({
  setSearchText,
  setSelection,
  countryData,
  setLang,
}) {
  let continents = {};
  countryData.map((country, index) => {
    if (country.continents[0] !== undefined) {
      continents[country.continents[0]] = index;
    }
  });
  return (
    <div className="SearchAndFilter">
      <div>
        <FontAwesomeIcon icon={faMagnifyingGlass} />
        <input
          type="text"
          placeholder="Search for a country..."
          onChange={(event) => {
            setSearchText(event.target.value);
          }}
        />
      </div>

      <Languages setLang={setLang} countries={countryData} />

      <select
        defaultValue={"Filter By Region"}
        onChange={(event) => {
          setSelection(event.target.value);
        }}>
        <option value="">Filter By Region</option>
        {continents &&
          Object.keys(continents).map((region, index) => {
            return (
              <option key={index} value={region}>
                {region}
              </option>
            );
          })}
      </select>
    </div>
  );
}
export default SearchAndFilter;
