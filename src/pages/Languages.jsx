import { Link } from "react-router-dom";

export default function Languages(props) {
  let countriesByLanguage = {};
  props.countries.map((country) => {
    let langs =
      country.languages &&
      Object.values(country.languages).map((lang) => {
        if (countriesByLanguage[lang]) {
          countriesByLanguage[lang].push(country.cca3);
        } else {
          countriesByLanguage[lang] = [country.cca3];
        }
        return lang;
      });
  });

  return (
    <select
      defaultValue={"Filter By Region"}
      onChange={(event) => {
        props.setLang(event.target.value);
      }}>
      <option value="">Filter By Languages</option>
      {countriesByLanguage &&
        Object.keys(countriesByLanguage).map((lang) => {
          return (
            <option key={lang} value={lang}>
              {lang}
            </option>
          );
        })}
    </select>
  );
}
