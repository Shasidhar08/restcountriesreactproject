import { faArrowLeftLong } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Link, Route, Routes, useParams } from "react-router-dom";
import AllCountries from "./AllCountries";
import axios from "axios";

export default function Country() {
  const { id } = useParams();
  const [country, setCountry] = useState();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getCountry();
  }, [id]);
  async function getCountry() {
    try {
      console.log(id);
      let response = await axios.get(
        `https://restcountries.com/v3.1/alpha/${id}`
      );
      console.log(response.data[0]);
      setCountry(response.data[0]);
      setLoading(false);
    } catch (error) {
      setError(error);
    }
  }
  if (error) {
    console.log(error);
    return <h1>Some Error Occured</h1>;
  }
  if (loading) return <h1>Loading</h1>;
  else
    return (
      <>
        {country !== undefined && (
          <div className="unitCountry" key={id}>
            <Link relative="path" to="../../">
              <button>
                <FontAwesomeIcon icon={faArrowLeftLong} /> back
              </button>
            </Link>

            <div key={country.cca3} className="container">
              <img src={country.flags.svg} alt="" />
              <div className="information-all">
                <h2>{country.name.common}</h2>
                <div className="information">
                  <div className="info">
                    Native Name: <p>{country.name.common}</p>
                  </div>
                  <div className="info">
                    population: <p>{country.population}</p>
                  </div>
                  <div className="info">
                    Region: <p>{country.region}</p>
                  </div>
                  <div className="info">
                    Sub Region: <p>{country.capital}</p>
                  </div>
                  <div className="info">
                    Capital: <p>{country.capital}</p>
                  </div>

                  <div className="info top-level-domian">
                    top level domain:
                    {country.tld.map((domain) => (
                      <p key={domain}>{domain}</p>
                    ))}
                  </div>
                  <div className="info currencies">
                    currencies:
                    <p>
                      {country.currencies &&
                        Object.entries(country.currencies).length > 0 &&
                        Object.entries(country.currencies)[0][1].name}
                    </p>
                  </div>
                  <div className="info language">
                    language:
                    <p>
                      {country.languages &&
                        Object.entries(country.languages)[0][1]}
                    </p>
                  </div>
                </div>
                <div className="border-countries">
                  <h3>border countries</h3>
                  <div className="borders">
                    {country.borders &&
                      country.borders.map((border) => (
                        <Link key={border} to={`/country/${border}`}>
                          <button>{border}</button>
                        </Link>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </>
    );
}
