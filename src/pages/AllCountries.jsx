import CountryCard from "../components/CountryCard";
import SearchAndFilter from "../components/SearchAndFilter";
import { useEffect, useState } from "react";
import axios from "axios";
import Languages from "./Languages";

function AllCountries() {
  const [countryData, setCountryData] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [selection, setSelection] = useState("");
  const [isLoading, setLoading] = useState("true");
  const [error, setError] = useState(null);
  const [lang, setLang] = useState("");

  useEffect(() => {
    getCountriesData();
  }, [searchText, lang, selection]);

  async function getCountriesData() {
    try {
      let response = await axios.get("https://restcountries.com/v3.1/all");
      setCountryData(response.data);
      setLoading(false);
    } catch (error) {
      setError(error);
    }
  }

  let filteredCountries = countryData;
  if (selection !== "" || lang !== "" || searchText !== "") {
    filteredCountries = countryData.filter((country) => {
      return (
        country.name.common.toLowerCase().includes(searchText.toLowerCase()) &&
        (selection === ""
          ? true
          : country.continents
          ? country.continents.includes(selection)
          : false) &&
        (lang === ""
          ? true
          : country.languages
          ? Object.values(country.languages).includes(lang)
          : false)
      );
    });
  }

  if (error) {
    return <h1>Some Error Occured</h1>;
  } else
    return (
      <div className="All">
        <SearchAndFilter
          setSearchText={setSearchText}
          setSelection={setSelection}
          countryData={countryData}
          setLang={setLang}
        />

        <div className="countries">
          {isLoading ? (
            <h1 className="unsolved">Loading...</h1>
          ) : (
            !isLoading &&
            filteredCountries.map((country, index) => {
              return <CountryCard key={index} country={country} />;
            })
          )}
        </div>
      </div>
    );
}
export default AllCountries;
