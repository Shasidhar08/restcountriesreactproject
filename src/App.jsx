import "./App.css";
import { Route, Routes } from "react-router-dom";
import Header from "./components/Header";
import AllCountries from "./pages/AllCountries";
import Country from "./pages/Country";
import Languages from "./pages/Languages";
function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<AllCountries />} />
        <Route path="/country/:id" element={<Country />} />
      </Routes>
    </>
  );
}
export default App;
